import { Component, ElementRef, ViewChild } from '@angular/core';
import  html2canvas from 'html2canvas';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent  {
  title = 'image-cropper-example';
  dataImg:any;

  constructor (){}

  pdfSrc="../assets/dummy2.pdf";

  @ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;

  downloadImage(){
  	window.scrollTo(0,0);  
    html2canvas(document.body,{scale: 5}).then(canvas => {
      this.canvas.nativeElement.src = canvas.toDataURL();
      this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      this.dataImg = this.downloadLink.nativeElement.href;
      console.log("href",this.downloadLink.nativeElement.href)
      // this.downloadLink.nativeElement.download = 'marble-diagram.png';
      // this.downloadLink.nativeElement.click();
    });
  }
}